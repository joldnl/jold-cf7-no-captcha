# Contact Forms 7 - No reCaptcha

## Description
Hide the reCaptcha on all pages with Contact Forms 7


## Features
* When activated, this plugin hides the reCaptcha on all pages throughout the site.


## Installation:

Add repository to your local composer file:

    {
        "type": "git",
        "url": "https://joldnl@bitbucket.org/joldnl/jold-cf7-no-captcha.git"
    }



Add the plugin as a depenency:

    "joldnl/jold-cf7-no-captcha": "~1.0",


Update composer by running:

    $ composer update.
