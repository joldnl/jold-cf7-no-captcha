<?php
/**
 * Plugin Name:            Contact Forms 7 - No reCaptcha
 * Description:            Hide the reCaptcha on all pages with Contact Forms 7
 * Version:                1.0.0
 * Plugin URI:             https://bitbucket.org/joldnl/jold-cf7-no-captcha
 * Bitbucket Plugin URI:   https://bitbucket.org/joldnl/jold-cf7-no-captcha
 * Bitbucket Branch:       master
 * Author:                 Jurgen Oldenburg
 * Author URI:             https://www.jold.nl
 */

// Include plugin.php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

function jold_cf7_norecaptcha() {
    // Only echo the css code if Contact Forms 7 plugin is activated
    if (is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) == true) {
        echo "<style>.grecaptcha-badge {opacity:0 !important}</style>\n";
    }
}

add_action('wp_head', 'jold_cf7_norecaptcha', 100);
add_action('login_head', 'jold_cf7_norecaptcha', 100);
add_action('admin_head', 'jold_cf7_norecaptcha', 100);
